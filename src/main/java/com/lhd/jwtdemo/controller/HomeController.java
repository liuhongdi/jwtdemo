package com.lhd.jwtdemo.controller;

import com.lhd.jwtdemo.result.Result;
import com.lhd.jwtdemo.util.SessionUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/home")
public class HomeController {

    //session详情
    @GetMapping("/home")
    @ResponseBody
    public Result home() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", SessionUtil.getCurrentUserName());
        data.put("userid", String.valueOf(SessionUtil.getCurrentUser().getUserid()));
        data.put("nickname", SessionUtil.getCurrentUser().getNickname());
        data.put("roles", SessionUtil.getCurrentUser().getAuthorities().toString());
        return Result.success(data);
    }
}